var React = require('react');
var IndexLink = require('react-router').IndexLink;
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      books: AppStore.getAllBooks()
    }
  },

  componentDidMount: function () {
    AppActions.getBooks();
    AppStore.addChangeListener(this._onChange)
  },

  componentWillUnmount: function () {
    AppStore.removeChangeListener(this._onChange);
  },

  _onChange: function () {
    this.setState({
      books: AppStore.getAllBooks()
    });
  },

  _renderChildren: function () {
    return React.Children.map(this.props.children, (function (child) {
      return React.cloneElement(child, {
        books: this.state.books
      });
    }).bind(this));
  },

  render: function () {
    return (
      <div>
        <header className="well container">
          <h4>Example 1 - Minimal Front-End App</h4>
          <a target="_block" href="https://bitbucket.org/workslon/master-thesis/wiki/chapter3.pdf">Related Thesis Chapter</a>
          &nbsp;/&nbsp;
          <a target="_block" href="https://bitbucket.org/workslon/master-example-1/src">Source Code</a>
        </header>

        <div className="page-header">
          <h1><IndexLink to="/">Public Library</IndexLink></h1>
        </div>
        { this._renderChildren() }
      </div>
    );
  }
});

