var React = require('react');
var Link = require('react-router').Link;
var Book = require('./Book.react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');

module.exports = React.createClass({
  render: function () {
    return (
      <div>
        <Link className="create-book btn btn-success bt-sm" to="/create">+ Add Book</Link>
        {this.props.books.length ?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>ISBN</th>
                <th>Title</th>
                <th>Year</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.books.map((function (book, i) {
                return (
                  <Book key={i} nr={i + 1} book={book} />
                );
              }).bind(this))}
            </tbody>
          </table>
          : <div>Loading books...</div>}
      </div>
    );
  }
});