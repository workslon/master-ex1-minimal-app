var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  componentWillMount: function () {
    this.book = AppStore.getBook(this.props.params.id);
  },

  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.isbn).focus();
  },

  update: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};

    AppActions.updateBook(this.book, {
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value)
    });
  },

  render: function () {
    return (
        <div>
          <h3>Update Book</h3>
          {this.book ?
            <form>
              <div className="form-group">
                <label htmlFor="isbn">ISBN</label>
                <input defaultValue={this.book.isbn} ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
              </div>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input defaultValue={this.book.title} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
              </div>
              <div className="form-group">
                <label htmlFor="year">Year</label>
                <input defaultValue={this.book.year} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
              </div>
              <button type="submit" onClick={this.update} className="btn btn-default">Submit</button>
              <IndexLink className="back" to="/">&laquo; back</IndexLink>
            </form>
          : <div>No book found...</div>}
        </div>
    );
  }
});