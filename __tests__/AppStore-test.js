jest.dontMock('../src/js/stores/AppStore.js');

describe('AppStore', function () {
  var createBookSuccessAction = {
    type: 'BOOK_SAVE_SUCCESS',
    data: {
      isbn: '123456789X',
      title: 'Book',
      year: 2000
    },
    result: '{ "objectId": "XXX" }'
  };

  var updateBookSuccessAction = {
    type: 'BOOK_UPDATE_SUCCESS',
    data: {
      objectId: 'XXX',
      isbn: '123456789X',
      title: 'Book2',
      year: 2002
    }
  };

  var destroyBookSuccessAction = {
    type: 'BOOK_DESTROY_SUCCESS',
    data: {objectId: 'XXX'}
  };

  var bookValidationErrorAction = {
    type: 'BOOK_VALIDATION_ERROR',
    errors: {
      isbn: 'Please supply a valid ISBN in ISBN-10 format!',
      title: 'Title can\'t be an empty string and can contain maximum of 50 characters!',
      year: 'Please supply a valid year in the range from 1459 to the current one!'
    }
  };

  var nonUniqueISBNAction = {
    type: 'NON_UNIQUE_ISBN',
    errors: {
      isbn: 'The book with such ISBN already exists!'
    }
  };

  var clearNotificationsAction = {
    type: 'CLEAR_NOTIFICATIONS'
  };

  var AppDispatcher;
  var AppStore;
  var callback;

  beforeEach(function() {
    AppDispatcher = require('../src/js/dispatchers/AppDispatcher');
    AppStore = require('../src/js/stores/AppStore');
    callback = AppDispatcher.register.mock.calls[0][0];
  });

  // General
  it('registers a callback with the dispatcher', function() {
    expect(AppDispatcher.register.mock.calls.length).toBe(1);
  });

  it('initializes with no books items', function() {
    var books = AppStore.getAllBooks();
    expect(books).toEqual([]);
  });

  // Get All Books SUCCESS
  it('adds all retrieved books to the `books` array', function() {
    callback(createBookSuccessAction);

    var books = AppStore.getAllBooks();
    var keys = Object.keys(books);

    expect(keys.length).toBe(1);
    expect(books[0].objectId).toEqual('XXX');
    expect(books[0].isbn).toEqual('123456789X');
    expect(books[0].title).toEqual('Book');
    expect(books[0].year).toEqual(2000);
  });

  // Create Book SUCCESS
  it('creates a book item', function() {
    callback(createBookSuccessAction);

    var books = AppStore.getAllBooks();
    var keys = Object.keys(books);

    expect(keys.length).toBe(1);
    expect(books[0].objectId).toEqual('XXX');
    expect(books[0].isbn).toEqual('123456789X');
    expect(books[0].title).toEqual('Book');
    expect(books[0].year).toEqual(2000);
  });

  // Update Book SUCCESS
  it('updates a book item', function() {
    callback(createBookSuccessAction);

    var books = AppStore.getAllBooks();

    callback(updateBookSuccessAction);
    expect(books[0].objectId).toEqual('XXX');
    expect(books[0].isbn).toEqual('123456789X');
    expect(books[0].title).toEqual('Book2');
    expect(books[0].year).toEqual(2002);
  });

  // Destroy Book SUCCESS
  it('destroys a book item', function() {
    callback(createBookSuccessAction);
    callback(destroyBookSuccessAction);
    expect(AppStore.getAllBooks().length).toBe(0);
  });
});