var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var AppActions = require('../src/js/actions/AppActions');
var AppStore = require('../src/js/stores/AppStore');

jest.dontMock('../src/js/components/UpdateBook.react');

describe('UpdateBook.react', function () {
  var UpdateBook = require('../src/js/components/UpdateBook.react');

  it('shows "Loading book..." message if no book found', function () {
    var renderer = TestUtils.createRenderer();
    var result;
    var children;

    renderer.render(<UpdateBook params={{id: 'XXX'}} />);
    result = renderer.getRenderOutput();
    children = result.props.children;
    expect(children[1].props.children).toEqual('No book found...');
  });

  it('shows the form if the book was found', function () {
    var renderer = TestUtils.createRenderer();
    var result;
    var children;

    AppStore.getBook = function () {
      return {
        objectId: 'XXX',
        isbn: '123456789X',
        title: 'Book',
        year: 2000
      }
    };

    renderer.render(<UpdateBook params={{id: 'XXX'}} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    // ISBN label
    expect(children[1].props.children[0].props.children[0].type).toEqual('label');
    expect(children[1].props.children[0].props.children[0].props.children).toEqual('ISBN');
    // ISBN input
    expect(children[1].props.children[0].props.children[1].type).toEqual('input');
    expect(children[1].props.children[0].props.children[1].ref).toEqual('isbn');
    expect(children[1].props.children[0].props.children[1].props.defaultValue).toEqual('123456789X');
    expect(children[1].props.children[0].props.children[1].props.type).toEqual('text');
    // Title label
    expect(children[1].props.children[1].props.children[0].type).toEqual('label');
    expect(children[1].props.children[1].props.children[0].props.children).toEqual('Title');
    // Title input
    expect(children[1].props.children[1].props.children[1].type).toEqual('input');
    expect(children[1].props.children[1].props.children[1].ref).toEqual('title');
    expect(children[1].props.children[1].props.children[1].props.defaultValue).toEqual('Book');
    expect(children[1].props.children[1].props.children[1].props.type).toEqual('text');
    // Year label
    expect(children[1].props.children[2].props.children[0].type).toEqual('label');
    expect(children[1].props.children[2].props.children[0].props.children).toEqual('Year');
    // Year input
    expect(children[1].props.children[2].props.children[1].type).toEqual('input');
    expect(children[1].props.children[2].props.children[1].ref).toEqual('year');
    expect(children[1].props.children[2].props.children[1].props.defaultValue).toEqual(2000);
    expect(children[1].props.children[2].props.children[1].props.type).toEqual('text');
    // button
    expect(children[1].props.children[3].type).toEqual('button');
    expect(children[1].props.children[3].props.onClick).toBeDefined();
  });

  it('calls "AppActions.UpdateBook" when clicking on "Submit" button', function () {
    var renderer = TestUtils.createRenderer();
    var result;
    var children;

    AppStore.getBook = function () {
      return {
        objectId: 'XXX',
        isbn: '123456789X',
        title: 'Book',
        year: 2000
      }
    };

    renderer.render(<UpdateBook params={{id: 'XXX'}} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    // as the "UpdateBook" function uses native "e.preventDefault" we need to simulate it.
    var fakeEvent = {
      preventDefault: function () {}
    };

    children[1].props.children[3].props.onClick(fakeEvent);
    expect(AppActions.updateBook).toBeCalled();
  });
});